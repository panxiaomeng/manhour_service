from django.shortcuts import render, HttpResponse
import numpy as np
import pandas as pd
from pandas import Timedelta
import os

# handle the data
class TimeData:
    def __init__(self, origin_excel, target_in_building_excel, target_in_room_excel=None):
        self.card_machine_in_building_list = ["AC-6_D-4F-01进_RD-4F-01进", "AC-6_D-4F-02进_RD-4F-02进",
                                              "AC-12_D-4F-16进_RD-4F-16进", "AC-12_D-4F-15进_RD-4F-15进",
                                              "AC-8_D-4F-17_RD-4F-17进"]
        self.card_machine_out_building_list = ["AC-6_D-4F-01出_RD-4F-01出", "AC-6_D-4F-02出_RD-4F-02出",
                                               "AC-12_D-4F-16出_RD-4F-16出", "AC-12_D-4F-15出_RD-4F-15出",
                                               "AC-8_D-4F-17_RD-4F-17出"]
        self.card_machine_in_room_list = ["AC-7_D-4F-07_RD-4F-07进", "AC-8_D-4F-17_RD-4F-17进",
                                          "AC-1_D-3F-01_RD-3F-01进"]
        self.card_machine_out_room_list = ["AC-7_D-4F-07_RD-4F-07出", "AC-8_D-4F-17_RD-4F-17出",
                                           "AC-1_D-3F-01_RD-3F-01出"]
        self.card_machine_other_floor = ["AC-1_D-3F-01_RD-3F-01进", "AC-1_D-3F-01_RD-3F-01出"]
        self.__origin_excel = origin_excel
        self.__target_in_building = target_in_building_excel
        self.__target_in_room = target_in_room_excel
        self.__df = self.read_excel()
        self.__df = self.format_table()
        self.__data_length = self.__df.shape[0]
        self.__df['time'] = pd.to_datetime(self.df['日期'])
        self.__df['day'] = self.df['time'].dt.strftime('%D')
        self.__duration_in_building = {}
        self.__duration_in_room = {}
        self.__data_in_building_list = []
        self.__data_in_room_list = []

    @property
    def origin_excel(self):
        return self.__origin_excel

    @property
    def target_in_building(self):
        return self.__target_in_building

    @property
    def target_in_room(self):
        return self.__target_in_room

    @property
    def df(self):
        return self.__df

    @property
    def data_length(self):
        return self.__data_length

    @property
    def duration_in_building(self):
        return self.__duration_in_building

    @property
    def duration_in_room(self):
        return self.__duration_in_room

    @property
    def data_in_building_list(self):
        return self.__data_in_building_list

    @property
    def data_in_room_list(self):
        return self.__data_in_room_list

    @staticmethod
    def highlight_start_time(value):
        """
        :param value: 表格里面的元素, 如果大于中午12点, 就高亮显示
        :return:
        """
        if isinstance(value, str):
            return 'background-color: red'
        elif value > pd.to_datetime("12:00:00 pm").timetz():
            return 'background-color: yellow'
        else:
            return 'background-color: white'

    @staticmethod
    def highlight_worktime(value):
        """
        :param value: 表格里面的元素,工作时长, 如果小于8小时, 就高亮显示
        :return:
        """
        if not isinstance(value, float):
            return 'background-color: red'
        elif value < 8.0:
            return 'background-color: yellow'
        else:
            return 'background-color: white'

    @staticmethod
    def add_log(func):
        def wrapper(*args, **kw):
            print(f"开始执行 {func.__name__}")
            return func(*args, **kw)
        return wrapper

    @add_log
    def read_excel(self):
        """
        从原始数据excel中读取
        :return: 原始数据
        """
        df = pd.read_excel(self.origin_excel, sheet_name=0)
        return df

    @add_log
    def write_sheet(self, df, target_excel_file):
        """
        :param df: 要写入的数据表格
        :param target_excel_file: 目标excel 文件
        :return: na
        """
        # 给符合条件的高亮显示
        df.style.applymap(self.highlight_worktime, subset=pd.IndexSlice[['工作时长']]). \
            applymap(self.highlight_start_time, subset=pd.IndexSlice[['上班时间']]). \
            to_excel(target_excel_file, engine='openpyxl')
        print(df)

    def sort_date(self, df):
        """
        等待补充, 如果数据很乱的话
        :param df:
        :return:
        """
        pass

    def format_table(self):
        """
        删除不必要的列, 只保留有用的数据, 这样避免不同的数据列导致的影响
        删除其他楼层的刷卡信息, 导致的整体错误
        :return:
        """
        # 删除不必要的列
        for x in self.__df:
            if x not in ["编号", "姓名", "日期", "考勤点"]:
                self.__df = self.__df.drop(labels=x, axis=1)
        # 删除其他楼层的刷卡信息
        other_floor_rows = []
        for row in range(self.__df.shape[0] - 2):
            if self.__df.iat[row, 3] in self.card_machine_other_floor:
                other_floor_rows.append(row)
        self.__df = self.__df.drop(labels=other_floor_rows, axis=0)
        return self.__df

    @add_log
    def data_handling_in_room(self):
        """
        从原始数据中遍历, 记录进入房间和出房间的时间, 并计算, 重新写到新的表格中
        :return: none
        """
        for row in range(self.data_length):
            self.duration_in_room[self.df.iat[row, 0]] = {}

        swipe_cards_time = []
        try:
            for row in range(self.data_length):
                # 计算在工作房间里面的时间
                if self.df.iat[row, 3] in self.card_machine_in_room_list or \
                        self.df.iat[row, 3] in self.card_machine_out_room_list:
                    swipe_cards_time.append((self.df.iat[row, 2], self.df.iat[row, 3]))
                    self.duration_in_room[self.df.iat[row, 0]].update({self.df.iat[row, 5]: swipe_cards_time})

                if row < self.data_length - 1 and (
                        self.df.iat[row, 5] != self.df.iat[row + 1, 5] or
                        self.df.iat[row, 0] != self.df.iat[row + 1, 0]) or \
                        row == self.data_length - 1:
                    swipe_cards_time = []
                    if self.duration_in_room[self.df.iat[row, 0]] == {}:
                        continue
                    if self.duration_in_room[self.df.iat[row, 0]][self.df.iat[row, 5]][0][1] \
                            in self.card_machine_in_room_list:
                        time_in_room = self.duration_in_room[self.df.iat[row, 0]][self.df.iat[row, 5]][0][0]
                    else:
                        time_in_room = "no record"
                    if self.duration_in_room[self.df.iat[row, 0]][self.df.iat[row, 5]][-1][1] \
                            in self.card_machine_out_room_list:
                        time_out_room = self.duration_in_room[self.df.iat[row, 0]][self.df.iat[row, 5]][-1][0]
                    else:
                        time_out_room = "no record"
                    if time_in_room == "no record" or time_out_room == "no record":
                        during_room = "no record"
                        print(f"{self.df.iat[row, 1]} forgot to swipe ID card on {self.df.iat[row, 5]} when"
                                     f"enter into the room")
                    else:
                        # 如果中午12点之后来上班就不用减去午饭1小时时间
                        if time_in_room < pd.to_datetime(f"{self.df.iat[row, 5]} 12:00:00 pm"):
                            during_room = float("%.2f" % ((time_out_room - time_in_room - Timedelta("01:00:00")) /
                                                np.timedelta64(1, 'h')))
                        else:
                            during_room = float("%.2f" % ((time_out_room - time_in_room) / np.timedelta64(1, 'h')))
                    if not isinstance(time_in_room, str):
                        time_in_room = time_in_room.timetz()
                    if not isinstance(time_out_room, str):
                        time_out_room = time_out_room.timetz()
                    # 生成新的表格, 准备写入excel
                    self.data_in_room_list.append({"姓名": self.df.iat[row, 1],
                                                   "日期": self.df.iat[row, 5],
                                                   "上班时间": time_in_room,
                                                   "下班时间": time_out_room,
                                                   "工作时长": during_room})
        except Exception as e:
            print(e)
        # logger.info(self.data_in_room_list)
        # 集合所有数据, 目前只有一个表
        total_data = pd.concat([pd.DataFrame(self.data_in_room_list)], axis=0)
        # 写入表格
        self.write_sheet(total_data, self.target_in_room)

    @add_log
    def data_handling_in_building(self):
        """
        从原始数据中遍历每一条记录, 并记录进入公司和出公司的时间,并计算,重新写到新的表格中
        :return: none
        """
        for row in range(self.data_length):
            self.duration_in_building[self.df.iat[row, 0]] = {}

        try:
            for row in range(self.data_length):
                # 计算在大楼里面的时间
                # 如果时间和上一行不一样表示是新的一天的开始
                if row == 0 or \
                        self.df.iat[row, 5] != self.df.iat[row - 1, 5] or \
                        self.df.iat[row, 0] != self.df.iat[row - 1, 0]:
                    time_in_building = self.df.iat[row, 2] \
                        if self.df.iat[row, 3] in self.card_machine_in_building_list else "no record"
                # 如果时间和下一行不一样表示是一天的结束
                if row < self.data_length - 1 and (
                        self.df.iat[row, 5] != self.df.iat[row + 1, 5] or
                        self.df.iat[row, 0] != self.df.iat[row + 1, 0]) or \
                        row == self.data_length - 1:
                    time_out_building = self.df.iat[row, 2] \
                        if self.df.iat[row, 3] in self.card_machine_out_building_list else "no record"
                    if time_in_building == "no record" or time_out_building == "no record":
                        during_building = "no record"
                        print(f"{self.df.iat[row, 1]} forgot to swipe ID card on {self.df.iat[row, 5]} when "
                                     f"enter into the building")
                    else:
                        # 如果中午12点之后来上班就不用减去午饭1小时时间
                        if time_in_building < pd.to_datetime(f"{self.df.iat[row, 5]} 12:00:00 pm"):
                            self.duration_in_building[self.df.iat[row, 0]][self.df.iat[row, 5]] = \
                                "%.2f" % ((time_out_building - time_in_building - Timedelta("01:00:00")) /
                                          np.timedelta64(1, 'h'))
                        else:
                            self.duration_in_building[self.df.iat[row, 0]][self.df.iat[row, 5]] = \
                                "%.2f" % ((time_out_building - time_in_building) / np.timedelta64(1, 'h'))
                        during_building = float(self.duration_in_building[self.df.iat[row, 0]][self.df.iat[row, 5]])
                    if not isinstance(time_in_building, str):
                        time_in_building = time_in_building.timetz()
                    if not isinstance(time_out_building, str):
                        time_out_building = time_out_building.timetz()
                    # 生成新的表格, 准备写入excel
                    self.data_in_building_list.append({"姓名": self.df.iat[row, 1],
                                                       "日期": self.df.iat[row, 5],
                                                       "上班时间": time_in_building,
                                                       "下班时间": time_out_building,
                                                       "工作时长": during_building})
        except Exception as e:
            print(e)

        # logger.info(data_in_building_list)
        # 集合所有数据, 目前只有一个表
        total_data = pd.concat([pd.DataFrame(self.data_in_building_list)], axis=0)
        # 写入表格
        self.write_sheet(total_data, self.target_in_building)


def index(request):
    return HttpResponse("welcome")


def html(request):
    return render(request, "html.html")


def upload(request):
    # return the upload page
    if request.method == "GET":
        return render(request, 'upload.html')
    # delete the origin excel if it has
    file_origin = os.path.join("statistic/tables/", "origin.xlsx")
    if os.path.exists(file_origin):
        os.remove(file_origin)
    # write the data to the new file from the choose the file
    file_object = request.FILES.get("file_path")
    f = open(file_origin, mode='wb')
    for chunk in file_object.chunks():
        f.write(chunk)
    f.close()
    print(f.name)
    return HttpResponse("<h1> upload successful</h1><br><a href='http://localhost:8000/result/'>generate the result</a>")


def result(request):

    man_hour = TimeData("statistic/tables/origin.xlsx", "statistic/tables/target.xlsx")
    man_hour.data_handling_in_building()
    data_set = pd.read_excel("statistic/tables/target.xlsx")
    data = data_set.values[:, :]
    test_data = [["序号", "姓名", "日期", "上班时间", "下班时间", "工作时长"]]
    for line in data:
        ls = []
        for j in line:
            ls.append(j)
        test_data.append(ls)
    return render(request, "results.html", {"result": test_data})
